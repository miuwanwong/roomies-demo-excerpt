import { Request, Response, NextFunction } from 'express';
// import path from "path";

export function isLoggedIn(req: Request, res: Response, next: NextFunction) {
    if (req.session?.['user']) {
        next();
    } else {
        console.log("not yet login");
        res.redirect('/site-homepage.html'); 
    }
}