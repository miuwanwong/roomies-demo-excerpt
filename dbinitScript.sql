CREATE TABLE "users"(
  "id" SERIAL PRIMARY KEY,
  "username" VARCHAR(255) NOT NULL,
  "password" VARCHAR(255) NOT NULL,
  "mobile" VARCHAR(255),
  "email" VARCHAR(255) NOT NULL,
  "image" VARCHAR(255),
  "flower" INTEGER,
  "egg" INTEGER,
  "gender" VARCHAR(255),
  "nickname" VARCHAR(255),
  "age" VARCHAR(255),
  "introduction" TEXT,
  "education_level" VARCHAR(255),
  "occupation" VARCHAR(255),
  "language" VARCHAR(255),
  "ethnicity" VARCHAR(255),
  "preferred_location" VARCHAR(255),
  "budget_min" INTEGER,
  "budget_max" INTEGER,
  "rent_date" DATE,
  "rent_duration" INTEGER,
  "preferred_gender" VARCHAR(255),
  "religion" VARCHAR(255),
  "have_no_kids" BOOLEAN,
  "not_smoking" BOOLEAN,
  "have_no_pet" BOOLEAN,
  "not_smoking_text " VARCHAR(255),
  "have_no_kids_text" VARCHAR(255),
  "have_no_pet_text" VARCHAR(255),
  "created_at" DATE NOT NULL DEFAULT NOW(),
  "updated_at" DATE NOT NULL DEFAULT NOW(),
  "confirm_key" VARCHAR(255),
  "activated" BOOLEAN,
  UNIQUE("username")
);





CREATE TABLE "site"(
  "id" SERIAL PRIMARY KEY,
  "owner_id" INTEGER NOT NULL,
  "title" VARCHAR(255),
  "district" VARCHAR(255),
  "address" VARCHAR(255),
  "description" TEXT,
  "rent" INTEGER,
  "rooms" INTEGER,
  "years" INTEGER,
  "floor" INTEGER,
  "gross_area" INTEGER,
  "usable_area" INTEGER,
  "deposit" VARCHAR(255),
  "view" VARCHAR(255),
  "television" BOOLEAN,
  "air_con" BOOLEAN,
  "washing_machine" BOOLEAN,
  "water_heater" BOOLEAN,
  "microwave" BOOLEAN,
  "fridge" BOOLEAN,
  "bed" BOOLEAN,
  "map_x" REAL,
  "map_y" REAL,
  "created_at" DATE NOT NULL DEFAULT NOW(),
  "updated_at" DATE NOT NULL DEFAULT NOW()
);
ALTER TABLE "site"
ADD CONSTRAINT "site_owner_id_foreign" FOREIGN KEY("owner_id") REFERENCES "users"("id");

CREATE TABLE "site_image"(
  "id" SERIAL PRIMARY KEY,
  "site_id" INTEGER NOT NULL,
  "image" VARCHAR(255) NOT NULL,
  "cover_image" INTEGER
);
ALTER TABLE "site_image"
ADD CONSTRAINT "site_image_site_id_foreign" FOREIGN KEY("site_id") REFERENCES "site"("id");

CREATE TABLE "site_meeting"(
  "id" SERIAL PRIMARY KEY,
  "site_id" INTEGER NOT NULL,
  "availability" BOOLEAN NOT NULL,
  "date" DATE NOT NULL
);



CREATE TABLE "social_event"(
  "id" SERIAL PRIMARY KEY,
  "admin_id" INTEGER NOT NULL,
  "availability" BOOLEAN NOT NULL,
  "date" DATE NOT NULL,
  "rent_district" VARCHAR(255) NOT NULL,
  "meeting_district" INTEGER NOT NULL,
  "meeting_address" VARCHAR(255) NOT NULL,
  "type" VARCHAR(255) NOT NULL,
  "category" VARCHAR(255) NOT NULL,
  "description" TEXT NOT NULL,
  "map_x" INTEGER NOT NULL,
  "map_y" INTEGER NOT NULL,
  "created_at" DATE NOT NULL,
  "updated_at" DATE NOT NULL
);
CREATE TABLE "site_attendee"(
  "id" SERIAL PRIMARY KEY,
  "users_id" INTEGER NOT NULL,
  "meeting_id" INTEGER NOT NULL,
  "attended" BOOLEAN NOT NULL
);
CREATE TABLE "event_attendee"(
  "id" SERIAL PRIMARY KEY,
  "users_id" INTEGER NOT NULL,
  "event_id" INTEGER NOT NULL,
  "attended" BOOLEAN NOT NULL
);
CREATE TABLE "event_image"(
  "id" SERIAL PRIMARY KEY,
  "event_id" INTEGER NOT NULL,
  "image" VARCHAR(255),
  "cover_image" INTEGER
);
CREATE TABLE "event_message"(
  "id" SERIAL PRIMARY KEY,
  "users_id" INTEGER NOT NULL,
  "event_id" INTEGER NOT NULL,
  "content" TEXT NOT NULL,
  "image" VARCHAR(255),
  "created_at" INTEGER NOT NULL,
  "updated_at" INTEGER NOT NULL
);
CREATE TABLE "site_message"(
  "id" SERIAL PRIMARY KEY,
  "users_id" INTEGER NOT NULL,
  "meeting_id" INTEGER NOT NULL,
  "content" TEXT NOT NULL,
  "image" VARCHAR(255),
  "created_at" DATE NOT NULL,
  "updated_at" DATE NOT NULL
);

CREATE TABLE "flower_egg_record"(
  "id" SERIAL PRIMARY KEY,
  "source_user_id" INT,
  "target_user_id" INT
);

CREATE TABLE "blocked_user"(
  "id" SERIAL PRIMARY KEY,
  "source_user_id" INT,
  "target_user_id" INT
);


ALTER TABLE "flower_egg_record"
ADD CONSTRAINT "flower_egg_record_source_user_id" FOREIGN KEY("source_user_id") REFERENCES "users"("id");

ALTER TABLE "blocked_user"
ADD CONSTRAINT "blocked_user_source_user_id" FOREIGN KEY("source_user_id") REFERENCES "users"("id");


ALTER TABLE "social_event"
ADD CONSTRAINT "social_event_admin_id_foreign" FOREIGN KEY("admin_id") REFERENCES "users"("id");
ALTER TABLE "event_message"
ADD CONSTRAINT "event_message_users_id_foreign" FOREIGN KEY("users_id") REFERENCES "users"("id");
ALTER TABLE "event_attendee"
ADD CONSTRAINT "event_attendee_event_id_foreign" FOREIGN KEY("event_id") REFERENCES "social_event"("id");
ALTER TABLE "event_image"
ADD CONSTRAINT "event_image_event_id_foreign" FOREIGN KEY("event_id") REFERENCES "social_event"("id");
ALTER TABLE "event_message"
ADD CONSTRAINT "event_message_event_id_foreign" FOREIGN KEY("event_id") REFERENCES "social_event"("id");
ALTER TABLE "site_attendee"
ADD CONSTRAINT "site_attendee_users_id_foreign" FOREIGN KEY("users_id") REFERENCES "users"("id");
ALTER TABLE "site_attendee"
ADD CONSTRAINT "site_attendee_meeting_id_foreign" FOREIGN KEY("meeting_id") REFERENCES "site_meeting"("id");
ALTER TABLE "site_message"
ADD CONSTRAINT "site_message_meeting_id_foreign" FOREIGN KEY("meeting_id") REFERENCES "site_meeting"("id");
ALTER TABLE "site_meeting"
ADD CONSTRAINT "site_meeting_site_id_foreign" FOREIGN KEY("site_id") REFERENCES "site"("id");
ALTER TABLE "event_attendee"
ADD CONSTRAINT "event_attendee_users_id_foreign" FOREIGN KEY("users_id") REFERENCES "users"("id");

