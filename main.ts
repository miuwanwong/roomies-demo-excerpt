import express, { Request, Response, NextFunction } from "express";
import path from "path"
import expressSession from "express-session"
import moment from 'moment';
import multer from 'multer';
import { Client } from 'pg';
import dotenv from 'dotenv';
import http from 'http';
import { Server as SocketIO } from 'socket.io';
import { isLoggedIn } from "./guards";
import mailjet from "node-mailjet";
import { formatMessage, userJoin, getCurrentUser, userLeave, getRoomUsers } from './utils';


// connect to PSQL database

dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});

client.connect()


// config multer

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve('./uploads'));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})
export const upload = multer({ storage })


// config express server & socket.io

const app = express();
const server = new http.Server(app);
export const io = new SocketIO(server);
const botName = 'Roomies ChatBot';

io.on("connection", function (socket) {
    // console.log(`socket is connected. socket id: ${socket.id}`);

    socket.on('joinRoom', ({ username, room }) => {
        const user = userJoin(socket.id, username, room);

        socket.join(user.room);

        // Welcome current user
        socket.emit('message', formatMessage(botName, '未有啱心水嘅樓盤...喺度認識吓室友先﹗'))

        // Broadcast when a user connects
        socket.broadcast.to(user.room).emit('message', formatMessage(botName, `${user.username} 入咗嚟﹗`));

        // send users and room info
        io.to(user.room).emit('roomUsers', {
            room: user.room,
            users: getRoomUsers(user.room)
        });

    })


    // listen for chatMessage
    socket.on('chatMessage', (msg) => {
        const user = getCurrentUser(socket.id);

        io.to(user.room).emit('message', formatMessage(user.username, msg));
    })

    // Runs when client disconnects
    socket.on('disconnect', () => {
        const user = userLeave(socket.id);

        if (user) {
            io.to(user.room).emit('message', formatMessage(botName, `${user.username} 離開了`))

            // send updated users and room info
            io.to(user.room).emit('roomUsers', {
                room: user.room,
                users: getRoomUsers(user.room)
            });

        }

    })



});



export const email = mailjet.connect("cb046104953e847317064299e866d490", "45d7b67d1f71bf44b77cac9ed569f371");


// import routers

import { messageRoutes } from "./Routers/messageRoutes";
import { siteDetailRoutes } from "./routers/siteDetailRoutes";
import { siteMeetingsRoutes } from "./routers/siteMeetingRoutes";
import { findRoommateRoutes } from './routers/findRoommateRoutes';
import { attendeesRoutes } from './routers/attendeesRoutes';
import { userPageRoutes } from './routers/userPageRoutes';
import { userRoutes } from "./routers/userRoutes";
import { siteHomepageRoutes } from "./routers/siteHomepageRoutes";
import { mailRoutes } from "./routers/mailRoutes";
import { eventRoutes } from "./routers/eventRoutes";


// config session and log details
app.use(
    expressSession({
        secret: "meet your roomies",
        resave: true,
        saveUninitialized: true,
    })
);


// https://www.tabnine.com/academy/javascript/how-to-format-date/
app.use((req: Request, res: Response, next: NextFunction) => {
    console.log(`Request ${req.path}`);
    let now = new Date();
    let dateStringWithTime = moment(now).format('YYYY-MM-DD HH:MM:SS')
    if (req.session["counter"]) {
        req.session["counter"] += 1;
    } else {
        req.session["counter"] = 1;
    }
    console.log(`Date:${dateStringWithTime} [info] ip: [${req.ip}], counter: [${req.session["counter"]}], path: [${req.path}], method: [${req.method}]`);
    next();
});



// config multer and json handling in express

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// our logic

app.use("/user", userRoutes) 
app.use("/messages", messageRoutes)
app.use("/siteMeetings", siteMeetingsRoutes)
app.use("/siteDetail", siteDetailRoutes)
app.use("/attendees", attendeesRoutes)
app.use("/", findRoommateRoutes)
app.use("/user_page", userPageRoutes)
app.use("/", siteHomepageRoutes);
app.use("/mail", mailRoutes);
app.use("/event", eventRoutes);




app.use(express.static(path.join(__dirname, "public")));
app.use(express.static(path.join(__dirname, "uploads")));
app.use(isLoggedIn, express.static(path.join(__dirname, "protected"))); //gaga testing


app.use((req, res) => {
    res.sendFile(path.join(__dirname, "public/404.html"));
});

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`listening to ${PORT} port`);
})