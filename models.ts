export interface Message {
    id: number;
    content: string;
}

export type MessageDataset = Array<Message>;


export interface User {
    username: string;
    password: string;
}

export type UserDataset = Array<User>;