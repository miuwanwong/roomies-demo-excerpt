// config responsive 3 column layout
const sidebarFirst = document.querySelector(".sidebar-first");
const mobileSidebarFirst = document.querySelector(".mobile-sidebar-first");
const closeButton = document.querySelector(".close-button");

mobileSidebarFirst.addEventListener("click", filterMenu);

function filterMenu() {
  sidebarFirst.classList.toggle("active");
  if (sidebarFirst.classList.contains("active")) {
    mobileSidebarFirst.classList.add("close");
    closeButton.classList.add("active");
  }
}

closeButton.addEventListener("click", closeFilterMenu);

function closeFilterMenu() {
  sidebarFirst.classList.remove("active");
  mobileSidebarFirst.classList.remove("close");
  closeButton.classList.remove("active");
}

// display all data using AJAX

showAllUsers();

async function showAllUsers() {
  const res = await fetch("/find-roommate"); // default method: GET
  // if server: res.json(XXX);
  // then data = XXX
  // { result: [{...}, {...}] }
  const result = await res.json();
  displayProfile(result);
  sendEmailButton(result);
}

function displayProfile(result) {
  let htmlStr = ``;
  let htmlStrNewRegistered = ``;

  for (const userProfile of result.data) {
    if (userProfile.id) {
      console.log(userProfile.username);
      const imgStr = /*html*/ `<img src=${userProfile.image} alt="" />`;
      htmlStrNewRegistered += /* html*/ `
  <a class="user-profile-box" id='userID-${
    userProfile.id
  }' href='../user_page.html?userID=${userProfile.id}'>
  <div class="user-profile">
    <div class="img-container">${imgStr}</div>
    <div class="user-info">
      <div class="user-info-wrapper">
        <div class="info-small-container">
          <h3 id="user-profile-nickname-${userProfile.id}">${
        userProfile.username
      }</h3>
          <div class="info-small-container">
            <p id="user-profile-occupation-and-ethnicity-${
              userProfile.id
            }">來自${userProfile.ethnicity} | ${userProfile.age}</p>
            <p>從事${userProfile.occupation}行業的${userProfile.gender}生</p>
          </div>

        </div>
        <div class="rent-small-container">
          <div class="user-budget" id="user-profile-budget-${
            userProfile.id
          }"> 預計租金 HKD ${userProfile.budget_min} 至 HKD
            ${userProfile.budget_max}</div>
          <div class="user-area" id="user-profile-location-${
            userProfile.id
          }"> 心儀地區：${userProfile.preferred_location}
          </div>
        </div>
      </div>

      <div class="user-info-wrapper">
        <div class="user-special-text-container">
          <div id="user-profile-introduction-${userProfile.id}">${
        userProfile.introduction
      }</div>
          <span class="badge badge-secondary">不吸煙</span>
          <span class="badge badge-secondary">無孩子</span>
          <span class="badge badge-secondary">無寵物</span>
          <div class="user-label-container"><span class="badge badge-info">${
            userProfile.language
          }</span><span class="badge badge-info">${
        userProfile.religion
      }</span></div>
        </div>


        <div class="date-small-container">
          <div id="user-profile-rent-date-${userProfile.id}" class="user-date">
            起租日期：${moment(userProfile.rent_date).format(
              "YYYY-MM-DD"
            )}</div>
          <div id="user-profile-rent-duration-${
            userProfile.id
          }" class="user-duration">租住${userProfile.rent_duration}個月</div>
        </div>
      </div>
      <div class="user-info-wrapper">
        <span class="badge badge-light">${moment(userProfile.created_at).format(
          "YYYY-MM-DD"
        )} 加入</span>
        <button class="btn btn-primary btn-sm" id="email-button-${
          userProfile.id
        }">發送興趣</button>
      </div>

    </div>
  </div>
</a>
`;
    }

    const imgStr = /*html*/ `<img src=${userProfile.image} alt="" />`;
    htmlStr += /* html*/ `
    <a class="user-profile-box" id='userID-${
      userProfile.id
    }' href='../user_page.html?userID=${userProfile.id}'>
    <div class="user-profile">
      <div class="img-container">${imgStr}</div>
      <div class="user-info">
        <div class="user-info-wrapper">
          <div class="info-small-container">
            <h3 id="user-profile-nickname-${userProfile.id}">${
      userProfile.username
    }</h3>
            <div class="info-small-container">
              <p id="user-profile-occupation-and-ethnicity-${
                userProfile.id
              }">來自${userProfile.ethnicity} | ${userProfile.age}</p>
              <p>從事${userProfile.occupation}行業的${userProfile.gender}生</p>
            </div>
  
          </div>
          <div class="rent-small-container">
            <div class="user-budget" id="user-profile-budget-${
              userProfile.id
            }"> 預計租金 HKD ${userProfile.budget_min} 至 HKD
              ${userProfile.budget_max}</div>
            <div class="user-area" id="user-profile-location-${
              userProfile.id
            }"> 心儀地區：${userProfile.preferred_location}
            </div>
          </div>
        </div>
  
        <div class="user-info-wrapper">
          <div class="user-special-text-container">
            <div id="user-profile-introduction-${userProfile.id}">${
      userProfile.introduction
    }</div>
            <span class="badge badge-secondary">不吸煙</span>
            <span class="badge badge-secondary">無孩子</span>
            <span class="badge badge-secondary">無寵物</span>
            <div class="user-label-container"><span class="badge badge-info">${
              userProfile.language
            }</span><span class="badge badge-info">${
      userProfile.religion
    }</span></div>
          </div>

  
          <div class="date-small-container">
            <div id="user-profile-rent-date-${
              userProfile.id
            }" class="user-date">
              起租日期：${moment(userProfile.rent_date).format(
                "YYYY-MM-DD"
              )}</div>
            <div id="user-profile-rent-duration-${
              userProfile.id
            }" class="user-duration">租住${userProfile.rent_duration}個月</div>
          </div>
        </div>
        <div class="user-info-wrapper">
          <span class="badge badge-light">${moment(
            userProfile.created_at
          ).format("YYYY-MM-DD")} 加入</span>
          <button class="btn btn-primary btn-sm" id="email-button-${
            userProfile.id
          }">發送興趣</button>
        </div>
  
      </div>
    </div>
  </a>
  `;
  }

  const userProfileContainer = document.querySelector(
    ".user-profile-container"
  );
  userProfileContainer.innerHTML = htmlStrNewRegistered + htmlStr;
}

// display filterred data using AJAX

// set rent_date current date always to today

window.onload = function () {
  const date = new Date();
  const currentDate = date.toISOString().substring(0, 10);

  document.getElementById("rent_date").value = currentDate;
  document.getElementById("rent_date").setAttribute("min", currentDate);
};

// set two-thumb slider

$(".nstSlider").nstSlider({
  crossable_handles: false,
  left_grip_selector: ".leftGrip",
  right_grip_selector: ".rightGrip",
  value_bar_selector: ".bar",
  value_changed_callback: function (cause, leftValue, rightValue) {
    $(this).parent().find(".leftLabel").text(leftValue);
    $(this).parent().find(".rightLabel").text(rightValue);
    $(".left-input").val(leftValue.toLocaleString());
    $(".right-input").val(rightValue.toLocaleString());
  },
  rounding: 1000,
});

$(document).ready(function () {
  $(".js-example-basic-multiple").select2();
});

// reset-filter button

const resetFilter = document.getElementById("reset-filter");
resetFilter.addEventListener("click", function () {
  filterForm.reset();
  showAllUsers();
});

// display filterred data

const filterForm = document.getElementById("filter-form");

filterForm.addEventListener("submit", async function (e) {
  e.preventDefault();
  const form = this;
  const formData = new FormData(e.currentTarget);

  const res = await fetch("/find-roommate-filter", {
    method: "POST",
    body: formData,
  });

  if (res.status === 200) {
    const result = await res.json();
    displayProfile(result);
    sendEmailButton(result);
  } else {
    const data = await res.json();
    const errMessage = data.message;
    alert(errMessage);
  }
});

$(() => {
  $(window).on("scroll", () => {
    let windowTop = $(window).scrollTop();
    if (windowTop > 100) {
      $("nav.navbar").addClass("active");
    } else {
      $("nav.navbar").removeClass("active");
    }
  });
});

/* --------------send email interest start------------------- */

// add event listener to email button

function sendEmailButton(result) {
  for (const userProfile of result.data) {
    document.getElementById(`email-button-${userProfile.id}`).onclick = (e) => {
      console.log("here");
      sendEmail("june.tecky@gmail.com", "June", "June");
      e.preventDefault();
    };
  }
}

async function sendEmail(receiverEmail, recieverName, userName) {
  console.log(receiverEmail);
  console.log(recieverName);
  console.log(userName);
  const res = await fetch("/mail/interest", {
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    body: JSON.stringify({
      receiverEmail: receiverEmail,
      recieverName: recieverName,
      userName: userName,
    }),
  });
  if (res.status == 200) {
    return true;
  } else {
    return false;
  }
}
/* --------------send email interest end------------------- */
