import moment from 'moment';

export function formatMessage(username: string, text: string) {
  return {
    username,
    text,
    time: moment().format('h:mm a')
  }
}

export const users: any = [];

// join user to chat
export function userJoin(id: string, username: string, room:string) {
  const user = {id, username, room};
  users.push(user);

  return user;
}

// Get current user
export function getCurrentUser(id: string) {
  return users.find((user: { id: string; }) => user.id === id);
}

// User leaves chat
export function userLeave(id: string) {
  const index = users.findIndex((user: { id: string; }) => user.id === id);

  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
}

// Get room users
export function getRoomUsers(room: string) {
  return users.filter((user: { room: string; }) => user.room === room);
}